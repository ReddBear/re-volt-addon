# Summary

[Introduction](./introduction.md)

- [Installation](./installation.md)
- [File Specifications](./files.md)
- [Tools Panel](./tools-panel.md)
    - [Import / Export](./tools-panel/io.md)
    - [Add-On Settings](./tools-panel/settings.md)
    - [Instances](./tools-panel/instances.md)
    - [Hulls](./tools-panel/hulls.md)
    - [Light and Shadow](./tools-panel/light-and-shadow.md)
    - [Helpers](./tools-panel/helpers.md)
    - [Face Properties](./tools-panel/face-properties.md)
    - [Vertex Colors](./tools-panel/vertex-colors.md)
    - [Texture Animation](./tools-panel/texture-animation.md)
- [Properties Editor](./properties.md)
    - [Object](./properties/object.md)
    - [Scene](./properties/scene.md)

[Changelog](./changelog.md)
