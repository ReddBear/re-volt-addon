# RV Add-On Documentation

## Download

<span style="float: right; margin: 1em 2em;">![](img/rva_small.png)</span>

This is the documentation for Marv's Add-On for Re-Volt files.  
It is intended to be used with [**Blender 2.79b**](https://www.blender.org/download/) or newer.  
_**Note**: Blender 2.8x series is not supported._

[**Download**](https://gitlab.com/re-volt/re-volt-addon/-/releases)


[Tutorial](https://re-volt.gitlab.io/content-tutorial)  
[Resources](https://re-volt.gitlab.io/content-tutorial/tracks/resources/index.html)


## Known Issues

Please report Bugs and suggest features on [GitLab](https://gitlab.com/re-volt/re-volt-addon/-/issues).

## Features

**Import, Export**

- World (.w)
- Mesh (.prm)
- Collision (.ncp)
- Instances (.fin)
- Mirror Planes (.rim)
- Hulls (.hul)
- Texture Animations (.ta.csv)
- Car Parameters (parameters.txt; only partial import: body, wheels)
- Track Zones (.taz)

**Missing Formats (help appreciated!)**

- Objects (.fob)
- AI Nodes (.fan)
- Position Nodes (.pos)
- Visiboxes (.vis)
- Lights (.lit)
- Force Fields (.fld)
- Level Information (.inf)
- Level properties (properties.txt)

**Tools**

- Re-Volt tool panel
    - Bake light to vertex color
    - Simple vertex painting tool
    - Car shadow generator
    - Texture animation helper
    - Convex hull generator
