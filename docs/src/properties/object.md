# Object

![Properties Editor: Object](./img/editor-object.png)

The Re-Volt Panel of the Object section is pinned to the top.


## NCP Properties

**Ignore Collision**: If enabled, the object won't be exported to `.ncp`.


## Instance Properties

![Instance Properties](./img/props-instance.png)

**Is Instance**: If enabled, the object will be exported to `.fin`. If a `.prm` file with the name of the object doesn't yet exist, it will also be exported to `.prm`.

**Model Color**: Additional or subtractive RGB color. Default is (0.5, 0.5, 0.5). Setting this to 1.0 erases the existing vertex color, 0.5 leaves it where it is and anything closer to 0 darkens the instance.

**EnvColor**: Environment map color and intensity (alpha).

**Hide**: Hides the instances (only collision).

**Don't show in Mirror Mode**: The instance won't be shown if the level is played in mirror mode.

**Is affected by Light**: In-game lights affect the instance.

**No Camera Collision**: The camera will clip through the instance.

**No Object Collision**: The instance won't collide with cars and other moving objects.

**Priority**: If set to anything other than `0`, the instance cannot be turned off using the video options.

**LoD Bias**: Unused.


## Mirror Properties

**Is Mirror Plane**: If enabled, the object will be exported to `.rim`.


## Hull Properties

These properties can be changed manually, however, it is encouraged to create hulls and spheres using the operators in the tools panel or via the _Add_ menu.

**Is Convex Hull**: If enabled, the object will be exported as a convex hull.

**Is Interior Sphere**: If enabled, the object will be exported as an interior sphere.

