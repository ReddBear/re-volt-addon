# Texture Animation

![Texture Animation Panel](./img/texanim.png)

An edit panel for texture animations. The animations themselves are saved in the
scene, not the selected object or faces. It is only accessible in edit mode to 
provide tools to use existing polygons and UV mapping to create an animation.

**Total Slots**:  
The total amount of texture animations you would like to use.  
For example, set this to `3` to use slots `0`, `1` and `2`.


## Animation Slots

**Slot**:  
The animation slot to display in the panel. This has nothing to do with the faces
currently selected, if any. The actual animation slot for a face is set by the
face's *texture page*.

**Frames**:  
The amount of frames you want to use for the animation. For example, set this to
`32` in order to use frames `0` to `31`.

**Animation Frame**:  

**Frame**:  
The frame to display in the panel.

**Texture**:  
The texture page number this animation frame uses.

**Duration**:  
The duration of the frame or the delay until the next frame shows up.

**Preview**:  
Click the Preview button to preview the frame's UV on the currently selected face.
The buttons next to it go back or advance one frame and then preview the UV of that
frame on the currently selected face.

> Do note that this feature is currently broken, because only the UV coordinates are
updated. The face is still rendered with its mapped texture, but this is really the
*animation slot* and not its actual texture.


## Animate

Functions for automatically generating animations.

### Transform Animation

![Texture Animation Panel: Transform](./img/texanim-transform.png)

Interpolates the UV coordinates between two given frames (Animates from point A to B),
and fills in values for the intermediate frames.

**Start Frame**:  
The start frame the animation starts from. This frame will not be changed.

**End Frame**:  
The frame the animation ends on. This frame will not be changed. To achieve a perfectly
looping animation, it's sometimes necessary to leave the last frame out. To do so,
decrease the amount of frames of the animation by 1.

**Frame Duration**:  
The duration of each resulting frame.

**Texture**:  
The texture page applied to each resulting frame.

### Grid Animation

![Texture Animation Panel: Grid](./img/texanim-grid.png)

Lays out animation frames on a grid, much like the mars animation in Museum 2.

**Start Frame**:  
The frame the animation starts at.

**X Resolution**:  
The width of the grid.

**Y Resolution**:  
The height of the grid.

**Frame Duration**:  
The duration of each resulting frame.

**Texture**:  
The texture page applied to each resulting frame.


## UV Helpers

**UV to Frame**:  
Takes the UV coordinates of the currently selected face and applies them to the texture
animation frame. You can use this to fill in the first / last frame data, then use
Grid or Transform functions above.

**Coordinates**:  
The UV mapping for the currently displayed frame. For triangular faces, UV3 will
be ignored.
